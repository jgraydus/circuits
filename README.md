## Digital Logic Circuit Simulator

A basic circuit consists of some wire objects combined with gates. For example, here's a program that creates a half adder:

    import Circuits

    main :: IO ()
    main = do
      -- create the input wires
      a <- newWire
      b <- newWire
      -- combine the inputs with an XOR gate to get the sum
      s <- newXor a b     -- yields a new wire: the output from the gate
      -- combine the inputs with an AND gate to get the carry bit
      c <- newAnd a b

      -- change the state of the input wires. changes propagate instantly through the circuit
      set a True          -- True = high voltage
      set b False         -- False = low voltage
      -- retrieve the state of the output wires
      s1 <- query s
      c1 <- query c

      putStrLn $ "The sum is: " ++ (show s1) ++ "\nThe carry is: " ++ (show c1)

The functions for creating gates are `newAnd`, `newOr`, `newNot`,
`newNand`, `newNor`, and `newXor`. Note that `newNot` only takes one argument.

These basic circuits are neat, but kind of boring. To do interesting stuff, we would like to create groups of wires that are packed together. The `Circuits.Bus` module provides some basic functions for creating and working with buses (groups of wires).

    import Circuits
    import Circuits.Bus

    main :: IO ()
    main = do
      -- create two 32 bit buses
      b1 <- newBus 32
      b2 <- newBus 32
      -- AND the two buses together
      out <- zipBus b1 b2 newAnd

      -- writeBus conveniently lets us set a bus value
      writeBus b1 7
      writeBus b2 12
      -- and likewise, readBus converts the bus value back to decimal
      v <- readBus out
      print v   -- will print 4

The `Circuits.Util` module provides `printTruthTable` to make it easier to examine the behavior of a circuit. Provide it with a list of input and a list of outputs (paired with single character names) and it will produce a truth table by trying every input combination, querying the output after each try.

    import Circuits
    import Circuits.Util
    
    main :: IO ()
    main = do
      a <- newWire
      b <- newWire
      c <- newAnd a b
      d <- newOr a b
      printTruthTable [(a,"A"),(b,"B")] [(c,"C"),(d,"D")]

This program will produce the following output:

    A  B  C  D
    ----------
    0  0  0  0
    0  1  0  1
    1  0  0  1
    1  1  1  1

More examples can be found in the file `src/Main.hs`.

## TODO:
 - finish ALU implementation
