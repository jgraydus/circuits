module Main where

import Control.Monad
import Control.Applicative
import Circuits
import Circuits.Multiplexor
import Circuits.Bus
import Circuits.Alu
import Circuits.Util

-- This example creates two inputs and connects them to an AND gate
-- The inputs are set to false and the output value is checked. Then
-- the first input is set to true and the output value is checked
-- again. Finally, the second input is set to true and the output
-- value is found to have changed to true just as expected.
demo1 :: IO ()
demo1 = do
  putStrLn ""
  putStrLn "## Demonstration 1: AND Gate ##"
  inA <- newWire
  inB <- newWire
  out <- newAnd inA inB
  putStrLn "> setting both inputs to False <"
  set inA False
  set inB False
  v1 <- query out
  putStrLn $ "output: " ++ (show v1)
  putStrLn "> setting input A to True <"
  set inA True
  v2 <- query out
  putStrLn $ "output: " ++ (show v2)
  putStrLn "> setting input B to True <"
  set inB True
  v3 <- query out
  putStrLn $ "output: " ++ (show v3)

-- This example creates a two input multiplexor and shows the result
-- of changing the value of the selector on the output value
demo2 :: IO ()
demo2 = do
  putStrLn ""
  putStrLn "## Demonstration 2: Multiplexor ##"
  inA <- newWire
  inB <- newWire
  inS <- newWire
  out <- multiplexor2 inA inB inS
  putStrLn "> setting input A to True and input B to False <"
  set inA True
  set inB False
  putStrLn "> setting the selector to False to choose input A <"
  set inS False
  v1 <- query out
  putStrLn $ "output: " ++ (show v1)
  putStrLn "> setting the selector to True to choose input B <"
  set inS True
  v2 <- query out
  putStrLn $ "output: " ++ (show v2)

-- in this example, two 32-bit width buses are created and connected
-- together via a "toggle switch" formed by connected each of the
-- corresponding lanes to a multiplexor. Every multiplexor shares the
-- same selector input, so changing the selector's value will
-- effectively toggle between the two buses.
demo3 :: IO ()
demo3 = do
  putStrLn ""
  putStrLn "## Demonstration 3: Switching Buses with Multiplexor ##"
  bus1 <- newBus 32
  bus2 <- newBus 32
  inS <- newWire
  out <- zipBus bus1 bus2 (\inA inB -> multiplexor2 inA inB inS)
  putStrLn "> setting the value on bus1 to 12345 <"
  writeBus bus1 12345
  putStrLn "> setting the value on bus2 to 54321 <"
  writeBus bus2 54321
  putStrLn "> setting the selector to False to choose bus1 <"
  set inS False
  v1 <- readBus out
  putStrLn $ "output:  " ++ (show v1)
  putStrLn "> setting the selector to True to choose bus2 <"
  set inS True
  v2 <- readBus out
  putStrLn $ "output:  " ++ (show v2)

reportAdderStatus :: Wire -> Wire -> IO ()
reportAdderStatus sum carry = do
  s <- query sum
  c <- query carry
  putStrLn $ "sum:   " ++ (show s)
  putStrLn $ "carry: " ++ (show c)

demo4 :: IO ()
demo4 = do
  putStrLn ""
  putStrLn "## Demonstration 4: Half Adder ##"
  a <- newWire
  b <- newWire
  (s,c) <- newHalfAdder a b
  putStrLn "> both inputs are False <"
  reportAdderStatus s c
  putStrLn "> setting first input to True <"
  set a True
  reportAdderStatus s c
  putStrLn "> setting second input to True <"
  set b True
  reportAdderStatus s c

demo5 :: IO ()
demo5 = do
  putStrLn ""
  putStrLn "## Demonstration 5: Full Adder ##"
  a <- newWire
  b <- newWire
  cIn <- newWire
  (s,cOut) <- newFullAdder a b cIn
  putStrLn "> all inputs are False <"
  reportAdderStatus s cOut
  putStrLn "> setting first input to True <"
  set a True
  reportAdderStatus s cOut
  putStrLn "> setting input carry to True <"
  set cIn True
  reportAdderStatus s cOut
  putStrLn "> setting second input to True <"
  set b True
  reportAdderStatus s cOut

-- in this demo, we create two 32 bit buses and feed them into an
-- adder. we then set the values on the buses and read the adder's
-- output. the writeBus function translates a decimal integer into a
-- binary value and sets the corresponding wires in the bus. the
-- readBus function translates the values of the wires in the bus back
-- to a decimal integer
demo6 :: IO ()
demo6 = do
  putStrLn ""
  putStrLn "## Demonstration 6: Adding Buses ##"
  bus1 <- newBus 32
  bus2 <- newBus 32
  (cIn,out,c) <- newBusAdder bus1 bus2
  putStrLn "> setting bus1 to 2468 <"
  writeBus bus1 2468
  putStrLn "> setting bus2 to 3210 <"
  writeBus bus2 3210
  v0 <- readBus out
  putStrLn $ "output: " ++ (show v0)
  putStrLn "> setting bus1 to 1111 <"
  writeBus bus1 1111
  v1 <- readBus out
  putStrLn $ "output: " ++ (show v1)

-- this example creates a full adder and prints out the truth table
-- showing all the different possible input states and their
-- corresponding outputs
demo7 :: IO ()
demo7 = do
  putStrLn ""
  putStrLn "## Demonstration 7: Generating a Truth Table ##"
  a <- newWire
  b <- newWire
  c <- newWire
  (s,c0) <- newFullAdder a b c
  putStrLn "Thruth table for full adder:"
  printTruthTable [(a,"A"),(b,"B"),(c,"C")] [(s,"S"),(c0,"C")]

mult4 [a,b,c,d] = multiplexor4 (a,b,c,d)

-- this demo shows how a 4-way mutiplexor can be used to select
-- between 4 buses
demo8 :: IO ()
demo8 = do
  putStrLn ""
  putStrLn "## Demonstration 8: 4-way Bus Selection ##"
  putStrLn "> creating four buses <"
  bs@[b1,b2,b3,b4] <- replicateM 4 (newBus 32)
  putStrLn "> combining the buses with a 4-way multiplexor <"
  [s1,s2] <- replicateM 2 newWire
  out <- zipBusN bs (flip mult4 (s1,s2))
  putStrLn "> writing 12345, 54321, 42, and 0 to the buses <"
  writeBus b1 12345
  writeBus b2 54321
  writeBus b3 42
  writeBus b4 0
  putStrLn "> selecting first bus <"
  set s1 False
  set s2 False
  r1 <- readBus out
  print r1
  putStrLn "> selecting second bus <"
  set s1 True
  r2 <- readBus out
  print r2
  putStrLn "> selecting third bus <"
  set s1 False
  set s2 True
  r3 <- readBus out
  print r3
  putStrLn "> selecting fourth bus <"
  set s1 True
  r4 <- readBus out
  print r4

main :: IO ()
main = do
  demo1
  demo2
  demo3
  demo4
  demo5
  demo6
  demo7
  
