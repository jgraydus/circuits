module Circuits.Util (printTruthTable) where

import Data.Traversable (traverse)
import Control.Monad (forM_)
import Data.List (intercalate)
import Circuits

{- printTruthTable -}

printTruthTable :: 
     [(Wire,String)]    -- the input wires paired with their names
  -> [(Wire,String)]    -- the output wires paired with their names
  -> IO ()
printTruthTable inputs outputs = do
  -- separate the names from the wires
  let (inputWires, inputNames) = unzip inputs
      (outputWires, outputNames) = unzip outputs
      allNames = inputNames ++ outputNames
  -- print the table header
  putStrLn $ intercalate "  " allNames
  putStrLn $ intercalate "--" (fmap (\_ -> "-") allNames)
  -- for each possible combination of input values
  forM_ (map reverse $ generateInputValues inputWires) $ \inputValues -> do
    -- set the input wires to the choice of values
    applyInputValues inputWires inputValues
    -- sample the output values
    outputValues <- (readAll outputWires)
    -- print a new row of the table
    putStrLn $ format inputValues outputValues

-- convert True to 1 and False to 0, then concatenate into a string
format :: [Bool] -> [Bool] -> String
format ins outs = intercalate "  " $ map (\b -> if b then "1" else "0") (ins ++ outs)

-- read the value of each of the given wires
readAll :: [Wire] -> IO [Bool]
readAll = traverse query

-- generates every possible combination of inputs
generateInputValues :: [a] -> [[Bool]]
generateInputValues [] = [[]]
generateInputValues (x:xs) = do
  v <- generateInputValues xs
  [False:v,True:v]

-- sets each wire to the value in the corresponding list of booleans
applyInputValues :: [Wire] -> [Bool] -> IO ()
applyInputValues ws bs = sequence_ $ zipWith set ws bs
