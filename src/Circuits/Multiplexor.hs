module Circuits.Multiplexor (multiplexor2, multiplexor4) where

import Circuits

-- a 2 input multiplexor. inA and inB are the inputs. inS is the selector
multiplexor2 :: Wire -> Wire -> Wire -> IO Wire
multiplexor2 inA inB inS = do
 not0 <- newNot inS
 and0 <- newAnd inA not0
 and1 <- newAnd inB inS
 newOr and0 and1

{- (s2,s1)    result
    (0,0)       a
    (0,1)       b
    (1,0)       c
    (1,1)       d     -}
multiplexor4 :: (Wire,Wire,Wire,Wire) -> (Wire,Wire) -> IO Wire
multiplexor4 (a,b,c,d) (s2,s1) = do
  m1 <- multiplexor2 a b s2
  m2 <- multiplexor2 c d s2
  multiplexor2 m1 m2 s1
  
