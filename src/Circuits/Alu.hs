module Circuits.Alu (newHalfAdder, newFullAdder, newBusAdder) where

import Circuits
import Circuits.Bus

newHalfAdder :: Wire -> Wire -> IO (Wire,Wire)
newHalfAdder a b = do
  s <- newXor a b -- sum
  c <- newAnd a b -- carry
  return (s,c)
  
newFullAdder :: Wire -> Wire -> Wire -> IO (Wire,Wire)
newFullAdder a b cIn = do
  (s1,c0) <- newHalfAdder a b
  (sOut,c1) <- newHalfAdder s1 cIn
  cOut <- newOr c0 c1
  return (sOut,cOut)

checkSize :: Int -> Int -> IO ()
checkSize n m = if n /= m then error "buses are different sizes" else return ()

-- return (carryIn, outputBus, carryOut)
newBusAdder :: Bus -> Bus -> IO (Wire, Bus, Wire)
newBusAdder (Bus bus1 n) (Bus bus2 m) = do
  checkSize n m
  cIn <- newWire 
  (ws,cOut) <- newBusAdder' bus1 bus2 cIn
  return (cIn, Bus ws n, cOut)

newBusAdder' [] _ c = return ([], c)
newBusAdder' (w1:ws1) (w2:ws2) c = do
  (s,c0) <- newFullAdder w1 w2 c
  (ss,c1) <- newBusAdder' ws1 ws2 c0
  return (s:ss,c1)

-- TODO the alu needs to take two input buses and some control imputs
-- and return a new bus. It should provide an and unit, an or unit, an
-- adder, an inverter on both inputs, a carry input, a subtract mode
-- that inverts an input and sets the carry input, a lessthan unit, an
-- equality unit, and... probably some other stuff
newAlu :: Bus -> Bus -> Wire -> Wire -> [Wire] -> IO Bus
newAlu a b binv cIn op = undefined

