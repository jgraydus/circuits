module Circuits.Bus (Bus(..),newBus, writeBus, readBus, zipBus, zipBusN) where

import Data.Word (Word)
import Data.Bits (testBit, setBit)
import Control.Monad (forM_, replicateM)
import Control.Monad.State (evalStateT, liftIO, get, put)
import Circuits (Wire, newWire, query, set)
import Data.List (transpose)

{- Bus -}

data Bus = Bus 
  { lanes :: [Wire]
  , size  :: Int
  }
  
newBus :: Int -> IO Bus
newBus n = do
  wires <- replicateM n newWire
  return $ Bus wires n

zipBus :: Bus -> Bus -> (Wire -> Wire -> IO Wire) -> IO Bus
zipBus (Bus b1 n) (Bus b2  _) f = do
  b3 <- sequence (zipWith f b1 b2)
  return $ Bus b3 n

writeBus :: Bus -> Word -> IO ()
writeBus (Bus ws n) w = do
  if w >= 2^n then error $ "value " ++ (show w) ++ " is too large for " ++ (show n) ++ " bit bus"
  else forM_ [0..(n-1)] (\i -> set (ws !! i) (testBit w i))

readBus :: Bus -> IO Word
readBus (Bus ws n) = evalStateT go (0,0)
  where go = do
             (i,w) <- get
             if i == n then
               return w
             else do
               b <- liftIO $ query (ws !! i)
               put (i+1, if b then setBit w i else w)
               go

-- combine n buses using a function that combines n wires
zipBusN :: [Bus] -> ([Wire] -> IO Wire) -> IO Bus
zipBusN buses f = do
  ws <- (sequence . map f . transpose . map lanes) buses
  return $ Bus ws (length ws)
